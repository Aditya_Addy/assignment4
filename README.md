1. The flask code is stored in run.py
2. templates folder contains all the html pages
3. static folder contains the css, image, js etc.
4. In order to use the page, first database needs to be populated by admin by starting the python server then typing:
'curl localhost:5000/addexp' and 'curl localhost:5000/addquiz' into the terminal. This requires nltk library to be downloaded.
5. Once the database is populated the page can be used.
6. The Experiments database contains three rows, one for each corpus. The columns have the corpus content, answers of number of tokens and types, and the actual tokens and types.
7. The Experiment page lets user select corpus. After pressing 'Go' it shows the experiment and allows to submit answers. On pressing submit button answer is checked and correct answer along with reasoning is shown. Page takes care to ensure integer inputs.
8. The Quiz database contains 5 rows ie 5 questions. The columns store the question, and the correct answer for number of tokens and types.
9. Every time the Quizzes page is selected a random set of any 3 questions come up from the database.  On pressing submit button answer is checked and correct answer along with reasoning is shown. Page takes care to ensure integer inputs.
